/*
* Copyright 2018 Builders
*************************************************************
*Nome     : Customer.java
*Autor    : Matheus Torres
*Data     : Thu Apr 12 2018 00:21:30 GMT-0300 (-03)
*************************************************************
*/
package br.com.builders.treinamento.domain;

import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.exception.ErrorCodes;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@Builder
@Document(collection = "matheus_torres_customer")
public class Customer {
	
	@Id
    @NotBlank(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED)
	private String id;		

	private String crmId;

	private String baseUrl;
	
	private String name;

	private String login;
	
	public void parse(CustomerRequest request) {
		this.setCrmId(request.getCrmId());
		this.setBaseUrl(request.getBaseUrl());
		this.setName(request.getName());
		this.setLogin(request.getLogin());
	}	
}
