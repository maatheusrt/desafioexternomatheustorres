package br.com.builders.treinamento.logger.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import br.com.builders.treinamento.logger.Slf4Customer;
import br.com.builders.treinamento.service.CustomerService;

public class Slf4CustomerImpl extends Slf4Customer {
	
	private final static Logger log = LoggerFactory.getLogger(CustomerService.class);
	
	@Override
	protected void CustomerMethod(String id, String crmId, String baseUrl, String name, String login) {
	    log.info("Describe message: id={}, nono={}.", id, crmId);
	}

}
