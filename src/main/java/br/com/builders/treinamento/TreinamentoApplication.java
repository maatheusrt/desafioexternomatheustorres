/*
* Copyright 2018 Builders
*************************************************************
*Nome     : treinamentoApplication.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.builders.treinamento.config.SwaggerConfig;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.swagger2.configuration.Swagger2DocumentationConfiguration;

@EnableSwagger2
@SpringBootApplication
@ComponentScan(basePackages = {"br.com.builders.treinamento"})
public class TreinamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreinamentoApplication.class, args);
	}

	@Bean
	public Swagger2DocumentationConfiguration swagger2Config(){
		return new springfox.documentation.swagger2.configuration.Swagger2DocumentationConfiguration();
	}

	@Bean
	public SwaggerConfig swaggerConfig(){
		return new SwaggerConfig();
	}

	@Bean
	public ObjectMapper mapper() {
		return new ObjectMapper();
	}
}
