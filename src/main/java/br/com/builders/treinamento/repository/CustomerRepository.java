package br.com.builders.treinamento.repository;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.builders.treinamento.domain.Customer;

@Lazy
public interface CustomerRepository extends MongoRepository<Customer, String> {

	Customer findByLogin(String login);
}
