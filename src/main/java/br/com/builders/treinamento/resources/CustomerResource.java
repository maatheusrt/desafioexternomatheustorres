package br.com.builders.treinamento.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.APIException;
import br.com.builders.treinamento.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api")
@Api(value="Customer API")
@Slf4j
public class CustomerResource {
	
  @Autowired
  private CustomerService service;
  
  
  @ApiOperation(httpMethod = "GET", tags = "customers",
		        value="List all customers / containers (with essential data, enough for displaying a table)",
		        nickname = "listCustomers", produces = MediaType.APPLICATION_JSON )
  @ApiResponses(value = {
	  @ApiResponse(code = 200, message = "Search results matching criteria", response = Customer.class)})  
  @RequestMapping(value = "/customers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<List<CustomerResponse>> listCustomers() throws APIException {
    
    List<Customer> customers = service.getAllCustomers();
    List<CustomerResponse> response = new ArrayList<CustomerResponse>();
    for (Customer c : customers) {
        response.add(CustomerResponse.builder()
        		                     .id(c.getId())
        		                     .crmId(c.getId())
        		                     .baseUrl(c.getBaseUrl())
        		                     .name(c.getName())
        		                     .login(c.getLogin())
        		                     .build());
    }
    
    log.info(">> FIND ALL CUSTOMER RESPONSE = {}", response);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  
  @ApiOperation(httpMethod = "POST", tags = "system",
		        value="Creates new customer",
		        nickname = "createCustomer", consumes = MediaType.APPLICATION_JSON,
		        produces = MediaType.APPLICATION_JSON )
  @ApiResponses(value = {
	  @ApiResponse(code = 201, message = "Item created"),
      @ApiResponse(code = 400, message = "Invalid input, object invalid"),
	  @ApiResponse(code = 409, message = "An existing item(login) already exists")})
  @RequestMapping(value = "/customers", method = RequestMethod.POST,
                  consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
  @ResponseStatus(value = HttpStatus.CREATED)
  public ResponseEntity<?> createCustomer(@ApiParam(name = "customer", value = "Customer to create") @RequestBody CustomerRequest request,
		  					              UriComponentsBuilder uri)
		throws APIException {   
	  
	  Customer newCustomer = service.createCustomer(Customer.builder()
			                        .crmId(request.getCrmId())
			                        .baseUrl(request.getBaseUrl())
			                        .name(request.getName())
			                        .login(request.getLogin())
			                        .build());
	  
	  UriComponents uriComponents = uri.path("/api/customers/{id}")
			  .buildAndExpand(newCustomer.getId());

	  log.info(">> CREATE CUSTOMER REQUEST = {}, CREATED = {}", request, newCustomer);
	  return ResponseEntity.created(uriComponents.toUri()).build();
  }

  
  @ApiOperation(httpMethod = "GET", tags = "customers",
		        value="List all data about a customer",
		        nickname = "getCustomer", produces = MediaType.APPLICATION_JSON )
  @ApiResponses(value = {
	  @ApiResponse(code = 200, message = "A single customer", response = Customer.class),
	  @ApiResponse(code = 404, message = "Customer with this ID not found")})	
  @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<CustomerResponse> getCustomer(@ApiParam(name = "customerId", value = "ID of customer that needs to be fetched", required = true) @PathVariable("customerId") String customerId)
	    throws APIException {
	  
	  Customer customer = service.getCustomerById(customerId);
	  CustomerResponse response = null;
	  
	  if (customer != null) {
	  response = CustomerResponse.builder()
			                                      .id(customer.getId())
			                                      .crmId(customer.getCrmId())
			                                      .baseUrl(customer.getBaseUrl())
			                                      .name(customer.getName())
			                                      .login(customer.getLogin())
			                                      .build();
	  }
	  
	  log.info(">> GET CUSTOMER ID = {}, RESPONSE = {}", customerId, response);
	  return new ResponseEntity<>(response, HttpStatus.OK);
  }

  
  @ApiOperation(httpMethod = "PUT", tags = "system",
		        value="Replaces a customer",
		        nickname = "replaceCustomer", consumes = MediaType.APPLICATION_JSON,
		        produces = MediaType.APPLICATION_JSON )
  @ApiResponses(value = {
	  @ApiResponse(code = 201, message = "Item replaced"),
	  @ApiResponse(code = 400, message = "Invalid input, object invalid"),
	  @ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found")})
  @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.PUT,
                  consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<Void> replaceCustomer(@ApiParam(name = "customerId", value = "ID of customer that needs to be replaced", required = true) @PathVariable("customerId") String customerId,
		                      				  @ApiParam(name = "customer", value = "Customer to replace") @RequestBody CustomerRequest request)
		throws APIException {
	  
      service.replaceCustomer(customerId, Customer.builder()
                                                  .crmId(request.getCrmId())
                                                  .baseUrl(request.getBaseUrl())
                                                  .name(request.getName())
                                                  .login(request.getLogin())
                                                  .build());      

	  log.info(">> REPLACE CUSTOMER ID = {}, REQUEST = {}", customerId, request);
      return new ResponseEntity<>(HttpStatus.OK);
  }

  
  @ApiOperation(httpMethod = "PATCH", tags = "system",
		        value="Modifies a customer",
		        nickname = "modifyCustomer", consumes = MediaType.APPLICATION_JSON,
		        produces = MediaType.APPLICATION_JSON )
  @ApiResponses(value = {
	  @ApiResponse(code = 201, message = "Item modified"),
	  @ApiResponse(code = 400, message = "Invalid input, object invalid"),
	  @ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found")})
  @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.PATCH,
                  consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<Void> modifyCustomer(@ApiParam(name = "customerId", value = "ID of customer that needs to be modified", required = true) @PathVariable("customerId") String customerId,
		                     				 @ApiParam(name = "customer", value = "Customer data with one or more fields filled") @RequestBody CustomerRequest request)
	    throws APIException {
	    
      service.updateCustomer(customerId, Customer.builder()
              									 .crmId(request.getCrmId())
            									 .baseUrl(request.getBaseUrl())
            									 .name(request.getName())
            									 .login(request.getLogin())
            									 .build());	
      
	 log.info(">> MODIFY CUSTOMER ID = {}, RESQUEST = {}", customerId, request);	  	
     return new ResponseEntity<>(HttpStatus.OK);
  }
  
  
  @ApiOperation(httpMethod = "DELETE", tags = "system",
	        value="Deletes a customer",
	        nickname = "deleteCustomer", consumes = MediaType.APPLICATION_JSON,
	        produces = MediaType.APPLICATION_JSON )
  @ApiResponses(value = {
  @ApiResponse(code = 201, message = "Item deleted"),
  @ApiResponse(code = 404, message = "The customer is not found")})	  
  @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
  @ResponseStatus(value = HttpStatus.OK)
  public ResponseEntity<Void> deleteCustomer(@ApiParam(name = "customerId", value = "ID of customer that needs to be deleted", required = true) @PathVariable("customerId") String customerId)
	    throws APIException {
	    
      service.deleteCustomer(customerId);

 	  log.info(">> DELETE CUSTOMER ID = {}", customerId);
      return new ResponseEntity<>(HttpStatus.OK);		  		  
  }

}
