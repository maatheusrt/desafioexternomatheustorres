package br.com.builders.treinamento.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "Customer", description = "Customer model.")
@ToString
@EqualsAndHashCode
public class CustomerResponse {

	@ApiModelProperty(value = "Internal Customer ID, uniquely identifying this customer in the world",
					  dataType = "string",
					  example = "553fa88c-4511-445c-b33a-ddff58d76886")
	private String id;

	@ApiModelProperty(value = "Customer ID in the CRM.",
            		  dataType = "string",
            		  example = "C645235")
    private String crmId;

	@ApiModelProperty(value = "Base URL of the customer container.",
  		  			  dataType = "string",
  			  	  	  reference = "url",
  		  			  example = "http://www.platformbuilders.com.br")
	private String baseUrl;

	@ApiModelProperty(value = "Customer name.",
  			  		  dataType = "string",
  			  		  example = "Platform Builders")
	private String name;

	@ApiModelProperty(value = "Admin login.",
	  		  		  dataType = "string",
	  	  			  reference = "email",
	  		  		  example = "contato@platformbuilders.com.br")
	private String login;
}
