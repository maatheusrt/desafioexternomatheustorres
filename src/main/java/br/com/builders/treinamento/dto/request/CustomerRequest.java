package br.com.builders.treinamento.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Tolerate;

@Builder
@Data
@ApiModel(value = "Customer", description = "Customer model.")
@ToString
@EqualsAndHashCode
public class CustomerRequest {

    private String crmId;

	private String baseUrl;

	private String name;

	private String login;
	
	@Tolerate
	public CustomerRequest() {
		this.crmId = "";
		this.baseUrl = "";
		this.name = "";
		this.login = "";
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}
