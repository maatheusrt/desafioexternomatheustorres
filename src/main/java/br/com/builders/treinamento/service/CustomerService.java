package br.com.builders.treinamento.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.APIException;
import br.com.builders.treinamento.exception.CustomerAlreadyExistsException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.UnprocessableEntityAPIException;
import br.com.builders.treinamento.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CustomerService {

	@Autowired
	private CustomerRepository repository;
	
	/*
	 * Create new customer only if login does not exist
	 */
	public Customer createCustomer(Customer customer) throws APIException{	
		try {
			
			if (customer != null) {
				Customer findCustomer = this.getCustomerByLogin(customer.getLogin());

				if (findCustomer != null)
					throw new CustomerAlreadyExistsException("This login already exists.");

				return repository.save(customer);
			} else {
				throw new UnprocessableEntityAPIException("Invalid customer data.");
			}
		} catch(CustomerAlreadyExistsException e) {
			log.error("Customer already exists.");
			throw e;
		} catch(UnprocessableEntityAPIException e2) {
			log.error("Invalid customer data.");
			throw e2;
		} catch(Exception ex) {
			log.error("Unknown error creating new customer.", ex);
			throw new UnprocessableEntityAPIException("Invalid input, object invalid.");			
		}
	}

	/*
	 * List all customers
	 */
	public List<Customer> getAllCustomers() throws APIException {
		return repository.findAll();
	}

	/*
	 * Get customer by ID
	 */
	public Customer getCustomerById(String id) throws APIException {
		Customer findCustomer;
		
		try { 
			findCustomer = repository.findOne(id);
		
			if (findCustomer == null)
				throw new NotFoundException("Could not find a customer with ID " + id + ".");
		} catch (NotFoundException e) {
			log.error("Customer not found with ID " + id + ".");
			throw e;
		} catch (Exception ex) {
			log.error("Unknown error searching for customer with this ID " + id + ".", ex);
			throw new UnprocessableEntityAPIException("Invalid customer object.");	
		}
		
		return findCustomer;
	}

	/*
	 * Get customer by ID
	 */
	public Customer getCustomerByLogin(String login) throws APIException {
		return repository.findByLogin(login);
	}

	/*
	 * Replace customer by ID
	 */
	public void replaceCustomer(String id, Customer customer) throws APIException{
		
		Customer findCustomer = null;
		
		try {			
			if (customer == null)
				throw new UnprocessableEntityAPIException("Invalid customer data.");
			
			findCustomer = repository.findOne(id);
			if (findCustomer == null)
				throw new NotFoundException("Could not find a customer with ID " + id + ".");
					
			repository.delete(findCustomer);
			repository.save(customer);

		} catch (UnprocessableEntityAPIException e1) {
			log.error("Invalid customer data.");
			throw e1;
		} catch (NotFoundException e2) {
			log.error("Customer not found with ID " + id + ".");
			throw e2;
		} catch (Exception ex) {			
			log.error("Unknown error replacing customer identified by ID " + id + ".", ex);
			throw new UnprocessableEntityAPIException("Invalid customer data.");			
		}
	}

	/*
	 * Update customer by ID
	 */
	public void updateCustomer(String id, Customer customer) throws APIException{
		
		Customer findCustomer = null;
		try{
			
			if (customer == null)
				throw new UnprocessableEntityAPIException("Invalid customer data.");	
			
			findCustomer = repository.findOne(id);
			if (findCustomer == null)
				throw new NotFoundException("Could not find a customer with ID " + id + ".");
			
			customer.setId(findCustomer.getId());
			if (customer.getCrmId() == null || customer.getCrmId().equals(""))
				customer.setCrmId(findCustomer.getCrmId());
			if (customer.getBaseUrl() == null || customer.getBaseUrl().equals(""))
				customer.setBaseUrl(findCustomer.getBaseUrl());
			if (customer.getName() == null || customer.getName().equals(""))
				customer.setName(findCustomer.getName());
			if (customer.getLogin() == null || customer.getLogin().equals(""))
				customer.setLogin(findCustomer.getLogin());
			
			repository.save(customer);

		} catch (UnprocessableEntityAPIException e1) {
			log.error("Invalid customer data.");
			throw e1;
		} catch (NotFoundException e2) {
			log.error("Customer not found with ID " + id + ".");
			throw e2;
		} catch (Exception ex) {			
			log.error("Unknown error updating customer identified by ID " + id + ".", ex);
			throw new UnprocessableEntityAPIException("Invalid customer data.");			
		}
	}

	/*
	 * Delete customer by ID
	 */
	public void deleteCustomer(String id) throws APIException{

		try{
			Customer customer = repository.findOne(id);
			if (customer == null)
				throw new NotFoundException("The customer is not found");
									
			repository.delete(customer);
			
		} catch (NotFoundException e){
			log.error("Customer not found with ID " + id + ".");
			throw e;
		} catch (Exception ex) {			
			log.error("Unknown error deleting customer identified by ID " + id + ".", ex);
			throw new UnprocessableEntityAPIException("Invalid customer data.");			
		}
	}
}