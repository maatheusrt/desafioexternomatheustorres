package br.com.builders.treinamento;

import java.util.Arrays;
import java.util.List;


import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import br.com.builders.treinamento.config.SimpleCORSFilter;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.CustomerAlreadyExistsException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.UnprocessableEntityAPIException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.resources.CustomerResource;
import br.com.builders.treinamento.service.CustomerService;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class})
public class TreinamentoApplicationTests {

    private MockMvc mockMvc;

    @Mock
    private CustomerService service;
    
    @Mock
    private CustomerRepository repository;

    @InjectMocks
	private CustomerResource resource;
	
	private final String ID_MOCK = "1";
	private final String CRMID_MOCK = "C645235";
	private final String BASE_URL_MOCK = "http://www.platformbuilders.com.br";
	private final String NAME_MOCK = "Platform Builders";
	private final String LOGIN_MOCK = "contato@platformbuilders.com.br";
	 
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(resource)
                .addFilters(new SimpleCORSFilter())
                .build();
    }
    
	private Customer buildCustomerMock(String id){		
		return buildCustomerMock(id, LOGIN_MOCK);
	}
    
	private Customer buildCustomerMock(String id, String login){		
		return Customer.builder()
				       .id(id)
				       .crmId(CRMID_MOCK)
				       .baseUrl(BASE_URL_MOCK)
				       .name(NAME_MOCK)
				       .login(login)
				       .build();
	}
    
	private Customer buildCustomerMockNoId(){		
		return Customer.builder()
				       .crmId(CRMID_MOCK)
				       .baseUrl(BASE_URL_MOCK)
				       .name(NAME_MOCK)
				       .login(LOGIN_MOCK)
				       .build();
	}

	@Test
	public void listCustomersTest() throws Exception {
		List<Customer> customers = Arrays.asList(buildCustomerMock(ID_MOCK),
				                       	 	     buildCustomerMock(ID_MOCK + "1", LOGIN_MOCK + "1"),
				                       	 	     buildCustomerMock(ID_MOCK + "2", LOGIN_MOCK + "2"));
		
		Mockito.when(service.getAllCustomers()).thenReturn(customers);

		mockMvc.perform(get("/api/customers"))
        			    .andExpect(status().isOk())
        			    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        			    .andExpect(jsonPath("$", hasSize(3)))
        			    .andExpect(jsonPath("$[0].id", is(ID_MOCK)))
        			    .andExpect(jsonPath("$[0].baseUrl", is(BASE_URL_MOCK)))
        			    .andExpect(jsonPath("$[0].name", is(NAME_MOCK)))
        			    .andExpect(jsonPath("$[0].login", is(LOGIN_MOCK)))
        			    .andExpect(jsonPath("$[1].id", is(ID_MOCK + "1")))
        			    .andExpect(jsonPath("$[1].baseUrl", is(BASE_URL_MOCK)))
        			    .andExpect(jsonPath("$[1].name", is(NAME_MOCK)))
        			    .andExpect(jsonPath("$[1].login", is(LOGIN_MOCK + "1")))
        			    .andExpect(jsonPath("$[2].id", is(ID_MOCK + "2")))
        			    .andExpect(jsonPath("$[2].baseUrl", is(BASE_URL_MOCK)))
        			    .andExpect(jsonPath("$[2].name", is(NAME_MOCK)))
        			    .andExpect(jsonPath("$[2].login", is(LOGIN_MOCK + "2")));

        verify(service, times(1)).getAllCustomers();
        verifyNoMoreInteractions(service);
		
	}
	
	@Test
	public void getCustomerByIdTest() throws Exception {
		Customer customer = buildCustomerMock(ID_MOCK);
		
		Mockito.when(service.getCustomerById(ID_MOCK)).thenReturn(customer);

		mockMvc.perform(get("/api/customers/{id}", ID_MOCK))
        			    .andExpect(status().isOk())
        			    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        			    .andExpect(jsonPath("$.id", is(ID_MOCK)))
        			    .andExpect(jsonPath("$.crmId", is(CRMID_MOCK)))
        			    .andExpect(jsonPath("$.baseUrl", is(BASE_URL_MOCK)))
        			    .andExpect(jsonPath("$.name", is(NAME_MOCK)))
        			    .andExpect(jsonPath("$.login", is(LOGIN_MOCK)));

        verify(service, times(1)).getCustomerById(ID_MOCK);
        verifyNoMoreInteractions(service);
	}
	
	@Test
	public void getCustomerByIdNotFoundTest() throws Exception {		
		Mockito.when(service.getCustomerById(ID_MOCK)).thenThrow(new NotFoundException("Customer not found with ID " + ID_MOCK + "."));

        Assertions.assertThatThrownBy(() -> mockMvc.perform(get("/api/customers/{id}", ID_MOCK))
        			    									.andExpect(status().isOk()))
											.hasCause(new NotFoundException("Customer not found with ID " + ID_MOCK + "."));

        verify(service, times(1)).getCustomerById(ID_MOCK);
        verifyNoMoreInteractions(service);
	}
	
	
	@Test
	public void createCustomerTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();
		
        Mockito.when(service.createCustomer(customer)).thenReturn(buildCustomerMock(ID_MOCK));

		mockMvc.perform(post("/api/customers").contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    .andExpect(status().isCreated())
        			    .andExpect(header().string("location", containsString("/api/customers/" + ID_MOCK)));

        verify(service, times(1)).createCustomer(customer);
        verifyNoMoreInteractions(service);
	}
	
	@Test
	public void createCustomerNullTest() throws Exception {
		Customer customer = buildCustomerMockNoId();
		
        Mockito.when(service.createCustomer(customer)).thenThrow(new UnprocessableEntityAPIException("Invalid customer data."));

        Assertions.assertThatThrownBy(() -> mockMvc.perform(post("/api/customers").contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    									.andExpect(status().isCreated())
        			    									.andExpect(header().string("location", containsString("/api/customers/" + ID_MOCK))))
                  							.hasCause(new UnprocessableEntityAPIException("Invalid customer data."));
	}
	
	@Test
	public void createCustomerAlreadyExistsTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();
		
        Mockito.when(service.createCustomer(customer)).thenThrow(new CustomerAlreadyExistsException("This login already exists."));

        Assertions.assertThatThrownBy(() -> mockMvc.perform(post("/api/customers").contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    									.andExpect(status().isCreated())
        			    									.andExpect(header().string("location", containsString("/api/customers/" + ID_MOCK))))
                  							.hasCause(new CustomerAlreadyExistsException("This login already exists."));
	}
	
	
	@Test
	public void replaceCustomerTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();

		Mockito.when(service.getCustomerById(ID_MOCK)).thenReturn(buildCustomerMock(ID_MOCK));
        Mockito.doNothing().when(service).replaceCustomer(ID_MOCK, customer);

		mockMvc.perform(put("/api/customers/{id}", ID_MOCK).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    .andExpect(status().isOk());

        verify(service, times(1)).replaceCustomer(ID_MOCK, customer);
        verifyNoMoreInteractions(service);
	}
	
	@Test
	public void replaceCustomerNullTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();

		Mockito.when(service.getCustomerById(ID_MOCK)).thenReturn(buildCustomerMock(ID_MOCK));
        Mockito.doThrow(new UnprocessableEntityAPIException("Invalid customer data.")).when(service).replaceCustomer(ID_MOCK, customer);

        Assertions.assertThatThrownBy(() -> mockMvc.perform(put("/api/customers/{id}", ID_MOCK).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    		   						    .andExpect(status().isOk()))
											.hasCause(new UnprocessableEntityAPIException("Invalid customer data."));
	}
	
	@Test
	public void replaceCustomerNotFoundTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();

		Mockito.when(service.getCustomerById(ID_MOCK)).thenReturn(buildCustomerMock(ID_MOCK));
        Mockito.doThrow(new NotFoundException("Customer not found with ID " + ID_MOCK + ".")).when(service).replaceCustomer(ID_MOCK, customer);

        Assertions.assertThatThrownBy(() -> mockMvc.perform(put("/api/customers/{id}", ID_MOCK).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    									.andExpect(status().isOk()))
											.hasCause(new NotFoundException("Customer not found with ID " + ID_MOCK + "."));
	}
	
	
	@Test
	public void modifyCustomerTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();

        Mockito.doNothing().when(service).updateCustomer(ID_MOCK, customer);

		mockMvc.perform(patch("/api/customers/{id}", ID_MOCK).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    .andExpect(status().isOk());

        verify(service, times(1)).updateCustomer(ID_MOCK, customer);
        verifyNoMoreInteractions(service);
	}	
	
	@Test
	public void modifyCustomerNullTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();

		Mockito.when(service.getCustomerById(ID_MOCK)).thenReturn(buildCustomerMock(ID_MOCK));
        Mockito.doThrow(new UnprocessableEntityAPIException("Invalid customer data.")).when(service).updateCustomer(ID_MOCK, customer);

        Assertions.assertThatThrownBy(() -> mockMvc.perform(patch("/api/customers/{id}", ID_MOCK).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    		   						    .andExpect(status().isOk()))
											.hasCause(new UnprocessableEntityAPIException("Invalid customer data."));
	}
	
	@Test
	public void modifyCustomerNotFoundTest() throws Exception {	
		Customer customer = buildCustomerMockNoId();

		Mockito.when(service.getCustomerById(ID_MOCK)).thenReturn(buildCustomerMock(ID_MOCK));
        Mockito.doThrow(new NotFoundException("Customer not found with ID " + ID_MOCK + ".")).when(service).updateCustomer(ID_MOCK, customer);

        Assertions.assertThatThrownBy(() -> mockMvc.perform(patch("/api/customers/{id}", ID_MOCK).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customer)))
        			    									.andExpect(status().isOk()))
											.hasCause(new NotFoundException("Customer not found with ID " + ID_MOCK + "."));
	}
	
	
	@Test
	public void deleteCustomerTest() throws Exception {
        Mockito.doNothing().when(service).deleteCustomer(ID_MOCK);

		mockMvc.perform(delete("/api/customers/{id}", ID_MOCK))
        			    .andExpect(status().isOk());

        verify(service, times(1)).deleteCustomer(ID_MOCK);
        verifyNoMoreInteractions(service);
	}	
	
	@Test
	public void deleteCustomerNotFoundTest() throws Exception {	
        Mockito.doThrow(new NotFoundException("Customer not found with ID " + ID_MOCK + ".")).when(service).deleteCustomer(ID_MOCK);

        Assertions.assertThatThrownBy(() -> mockMvc.perform(delete("/api/customers/{id}", ID_MOCK))
        			    									.andExpect(status().isOk()))
											.hasCause(new NotFoundException("Customer not found with ID " + ID_MOCK + "."));
	}

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
