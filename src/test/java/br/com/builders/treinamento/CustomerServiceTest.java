package br.com.builders.treinamento;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.APIException;
import br.com.builders.treinamento.exception.CustomerAlreadyExistsException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.UnprocessableEntityAPIException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {
	
	@Autowired
	private CustomerRepository repository;
	
	@Autowired
	private CustomerService service;
	
	private final String ID_MOCK = "553fa88c-4511-445c-b33a-ddff58d7688600";
	private final String CRM_ID_MOCK="C645235";
	private final String BASE_URL_MOCK="http://www.platformbuilders.com.br";
	private final String NAME_MOCK = "Platform Builders";
	private final String LOGIN_MOCK = "testcontato@platformbuilders.com.br";
	
	private Customer buildCustomerMock(String id){		
		return this.buildCustomerMock(id, LOGIN_MOCK);
	}
	
	private Customer buildCustomerMock(String id, String login){
		return Customer.builder()
				       .id(id)
				       .crmId(CRM_ID_MOCK)
				       .baseUrl(BASE_URL_MOCK)
				       .name(NAME_MOCK)
				       .login(login)
				       .build();
	}
	
	private void clearCustomer(String id) {
		Customer c = repository.findOne(id);
		
		if (c != null)
			repository.delete(c);
	}
	
	@Test
	public void getAllCustomersTest() throws APIException {
		String idTest = "553fa88c-4511-445c-b33a-ddff58d76887";
		String idTest2 = "553fa88c-4511-445c-b33a-ddff58d76888";

		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		customer = buildCustomerMock(idTest, LOGIN_MOCK + "X");
		clearCustomer(idTest);
		service.createCustomer(customer);

		customer = buildCustomerMock(idTest2, LOGIN_MOCK + "Z");
		clearCustomer(idTest2);
		service.createCustomer(customer);

		List<Customer> customers = service.getAllCustomers();

		assertTrue(customers.size() >= 3);

		clearCustomer(ID_MOCK);
		clearCustomer(idTest);
		clearCustomer(idTest2);
	}
	
	@Test
	public void createCustomerTest() throws APIException { 

		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		Customer customerVerify = service.getCustomerById(ID_MOCK);

		assertTrue(customer.equals(customerVerify));

		clearCustomer(ID_MOCK);
	}
	
	
	@Test(expected = CustomerAlreadyExistsException.class)
	public void createCustomerAlreadyExistsTest() throws APIException { 
		Customer customer = buildCustomerMock(ID_MOCK);
		service.createCustomer(customer);

		Customer customerVerify = buildCustomerMock(ID_MOCK);
		service.createCustomer(customerVerify);		
	}
	
	@Test(expected = UnprocessableEntityAPIException.class)
	public void createCustomerNullTest() throws APIException{ 
		service.createCustomer(null);
	}
	
	@Test
	public void getCustomerByIdTest() throws APIException {
		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		Customer customerVerify = service.getCustomerById(ID_MOCK);

		assertTrue(customer.equals(customerVerify));

		clearCustomer(ID_MOCK);
	}
	
	@Test(expected = NotFoundException.class)
	public void getCustomerByIdNotFoundTest() throws APIException {
		service.getCustomerById("000XXX000");
	}
	
	@Test
	public void replaceCustomerTest() throws APIException {
		String idTest = "553fa88c-4511-445c-b33a-ddff58d7688944";

		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		Customer customer2 = Customer.builder()
				.id(idTest)
				.baseUrl(BASE_URL_MOCK + BASE_URL_MOCK)
				.crmId(CRM_ID_MOCK + CRM_ID_MOCK)
				.login(LOGIN_MOCK + LOGIN_MOCK)
				.name(NAME_MOCK + NAME_MOCK)
				.build();

		service.replaceCustomer(ID_MOCK, customer2);

		Customer oldCustomer = repository.findOne(ID_MOCK);			
		assertNull(oldCustomer);

		Customer customerVerify = service.getCustomerById(idTest);
		assertTrue(customerVerify.getBaseUrl().equals(BASE_URL_MOCK + BASE_URL_MOCK));
		assertTrue(customerVerify.getCrmId().equals(CRM_ID_MOCK + CRM_ID_MOCK));
		assertTrue(customerVerify.getLogin().equals(LOGIN_MOCK + LOGIN_MOCK));
		assertTrue(customerVerify.getName().equals(NAME_MOCK + NAME_MOCK));

		clearCustomer(idTest);
	}
	
	@Test(expected = NotFoundException.class)
	public void replaceCustomerNotFoundTest() throws APIException {	
		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer("000XXX000");

		service.replaceCustomer("000XXX000", customer);
	}
	
	@Test(expected = UnprocessableEntityAPIException.class)
	public void replaceCustomerNullTest() throws APIException {
		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		service.replaceCustomer(ID_MOCK, null);
		
		clearCustomer(ID_MOCK);
	}
	
	@Test
	public void updateCustomerTest() throws APIException {			  
		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		Customer customer2 = Customer.builder()
				.baseUrl(BASE_URL_MOCK + "A")
				.crmId(CRM_ID_MOCK + "A")
				.login(LOGIN_MOCK + "A")
				.name(NAME_MOCK + "A")
				.build();

		service.updateCustomer(ID_MOCK, customer2);

		Customer customerVerify = service.getCustomerById(ID_MOCK);
		assertTrue(customerVerify.getBaseUrl().equals(BASE_URL_MOCK + "A"));
		assertTrue(customerVerify.getCrmId().equals(CRM_ID_MOCK + "A"));
		assertTrue(customerVerify.getLogin().equals(LOGIN_MOCK + "A"));
		assertTrue(customerVerify.getName().equals(NAME_MOCK + "A"));
		
		clearCustomer(ID_MOCK);
	}
	
	@Test
	public void updateCustomerAttributeTest() throws APIException {

		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		// Verify crmId update
		Customer customerCrmId = Customer.builder().crmId(CRM_ID_MOCK + "A").build();								
		service.updateCustomer(ID_MOCK, customerCrmId);

		Customer customerVerify = service.getCustomerById(ID_MOCK);

		assertTrue(customerVerify.getCrmId().equals(CRM_ID_MOCK + "A"));
		assertTrue(customerVerify.getBaseUrl().equals(BASE_URL_MOCK));
		assertTrue(customerVerify.getName().equals(NAME_MOCK));
		assertTrue(customerVerify.getLogin().equals(LOGIN_MOCK));


		// Verify baseUrl update
		Customer customerBaseUrl = Customer.builder().baseUrl(BASE_URL_MOCK + "A").build();								
		service.updateCustomer(ID_MOCK, customerBaseUrl);

		customerVerify = service.getCustomerById(ID_MOCK);
		assertTrue(customerVerify.getCrmId().equals(CRM_ID_MOCK + "A"));
		assertTrue(customerVerify.getBaseUrl().equals(BASE_URL_MOCK + "A"));
		assertTrue(customerVerify.getName().equals(NAME_MOCK));
		assertTrue(customerVerify.getLogin().equals(LOGIN_MOCK));


		// Verify name update
		Customer customerName = Customer.builder().name(NAME_MOCK + "A").build();								
		service.updateCustomer(ID_MOCK, customerName);

		customerVerify = service.getCustomerById(ID_MOCK);
		assertTrue(customerVerify.getCrmId().equals(CRM_ID_MOCK + "A"));
		assertTrue(customerVerify.getBaseUrl().equals(BASE_URL_MOCK + "A"));
		assertTrue(customerVerify.getName().equals(NAME_MOCK + "A"));
		assertTrue(customerVerify.getLogin().equals(LOGIN_MOCK));


		// Verify name update
		Customer customerLogin = Customer.builder().login(LOGIN_MOCK + "A").build();								
		service.updateCustomer(ID_MOCK, customerLogin);

		customerVerify = service.getCustomerById(ID_MOCK);
		assertTrue(customerVerify.getCrmId().equals(CRM_ID_MOCK + "A"));
		assertTrue(customerVerify.getBaseUrl().equals(BASE_URL_MOCK + "A"));
		assertTrue(customerVerify.getName().equals(NAME_MOCK + "A"));
		assertTrue(customerVerify.getLogin().equals(LOGIN_MOCK + "A"));

		clearCustomer(ID_MOCK);
	}
	
	@Test(expected = NotFoundException.class)
	public void updateCustomerNotFoundTest() throws APIException {
		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer("000XXX000");

		service.updateCustomer("000XXX000", customer);
	}
	
	@Test(expected = UnprocessableEntityAPIException.class)
	public void updateCustomerNullTest() throws APIException {
		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);	

		service.updateCustomer(ID_MOCK, null);

		clearCustomer(ID_MOCK);
	}
	
	@Test
	public void deleteCustomerTest() throws APIException {
		Customer customer = buildCustomerMock(ID_MOCK);
		clearCustomer(ID_MOCK);
		service.createCustomer(customer);

		service.deleteCustomer(ID_MOCK);
		
		Customer customerVerify = repository.findOne(ID_MOCK);
		assertNull(customerVerify);		
	}
	
	@Test(expected = NotFoundException.class)
	public void deleteCustomerNotFoundTest() throws APIException {
		service.deleteCustomer("000XXX000");
		service.deleteCustomer("000XXX000");
	}		
}